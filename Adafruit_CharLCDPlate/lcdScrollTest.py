#!/usr/bin/python
"""
lcdScrollTest.pyAuthor             :  Eric Pavey 
Creation Date      :  2014-02-08
Blog               :  http://www.akeric.com/blog

Free and open for all to use.  But put credit where credit is due.

OVERVIEW:-----------------------------------------------------------------------
Example showing scrolling text on a LCD display.  Designed to work on the the Adafruit LCD 
+ keypad, but it's not tied to any specific hardware, and should work on a LCD
of any size.

Authored using Adafruit WebIDE talking to Rasperry Pi over ssh

Hardware:
http://www.adafruit.com/products/1109
http://learn.adafruit.com/adafruit-16x2-character-lcd-plus-keypad-for-raspberry-pi
Source needed for imports:
https://github.com/adafruit/Adafruit-Raspberry-Pi-Python-Code/tree/master/Adafruit_CharLCDPlate
"""
from time import sleep
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate
from lcdScroll import Scroller
    
class App(Adafruit_CharLCDPlate):
    """
    Example app to display scrolling text.  Wrapper around the adafruit lcd object.
    """
    def __init__(self):
        Adafruit_CharLCDPlate.__init__(self)

        # setup the LCD:
        self.clear()
        self.backlight(self.WHITE)
        self.speed = .5 # How fast to scroll the lcd

        # Set some lines to display:
        lines=["Super awesome top line!", "The lower line, yah yah boom"]
        # Create our scroller instance:
        scroller = Scroller(lines=lines)
        # Display the first unscrolled message:
        message = "\n".join(lines)
        self.message(message)
        sleep(self.speed)
    
        # main loop:
        while True:
            # Get the updated scrolled lines, and display:
            message = scroller.scroll()
            self.clear()
            self.message(message)
            sleep(self.speed)
        
if __name__ == "__main__":
    App()

