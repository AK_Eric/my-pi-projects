#!/usr/bin/python
"""
raspberryFm01.py
Author             :  Eric Pavey 
Creation Date      :  2014-01-26
Project blog into  :  http://www.akeric.com/blog/?p=2484

Free and open for all to use.  But put credit where credit is due.

OVERVIEW:-----------------------------------------------------------------------
Simple internet radio turner for the Raspberry Pi & Adafruits LCD keypad.

Easy to add new streams via the Pi's shell, independent from this program.
Will auto-detect for internet, and hold until it shows up \ returns.
If setup via the instructions on the above blog link, will both auto-start with
    the pi, and provide pi shutdown ability.
Will remember the last station played and volume, and reset to that state on restart.
Can change volume & channel with keypad.  Can shutdown pi\program with keypad.

SOFTWARE REQIREMENTS:-----------------------------------------------------------
Uses the mpd player, and it's mpc player client for the audio streaming:
    $ sudo apt-get install mpc mpd
LCD sources needed for Python imports:
    https://github.com/adafruit/Adafruit-Raspberry-Pi-Python-Code/tree/master/Adafruit_CharLCDPlate

HARDWARE RQUIREMENTS:-----------------------------------------------------------
Raspberry Pi (I used version B)
Adafruit LCD+Keypad:
    http://www.adafruit.com/products/1109
    http://learn.adafruit.com/adafruit-16x2-character-lcd-plus-keypad-for-raspberry-pi
    Solder it up & plug it in!
Optional 3D printed case: http://www.thingiverse.com/thing:240487

USAGE:--------------------------------------------------------------------------
Adding new streams in the shell is easy.  May I suggest http://somafm.com/  ?
Streams are added manually via the below syntax.  Once added they are remembered.
mpc add http://ice.somafm.com/groovesalad
mpc add http://ice.somafm.com/lush
mpc add http://ice.somafm.com/deepspaceone
mpc add http://ice.somafm.com/dronezone
mpc add http://ice.somafm.com/spacestation
mpc add http://ice.somafm.com/sonicuniverse
mpc add http://ice.somafm.com/beatblender
mpc add http://ice.somafm.com/bootliquor
etc...
Other:
I Am Christian Contemporary:
mpc add http://68.168.103.13:8363

Buttons:
Up\Down adjust volume.
Left\Right adjust station.
Select displays below info.
Select+Left shuts down the Pi.
Select+Right exits the program.

UPDATES:------------------------------------------------------------------------
v1.01 : Added new features allowing the App to go into a holding pattern if 
    internet is not pesent, or if it looses connection during playback.
v1.02 : 2014-02-08 : Will now detect if MPC is playing, and not restart it.
v1.03 : 2014-02-09 : Adding text scrolling.  Will now loop stations when you get
    to the start or end.
v1.04 : 2014-02-12 : Updating MPC.getData() to do a bit more intelligent line 
    splitting on song data that didn't come from SomaFM.
v1.05 : 2014-10-12 : Adding exception info to getData() to try and track down
    failure to capture MPC data properly.
"""
import os
import re
import sys
import socket
import cPickle
from time import sleep
from subprocess import Popen, PIPE

# Note, this module also calls to Adafruit_I2C.py : you need that too.
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate

__version__ = "1.05"

# This file will save the volume and track # when quit:
STATE_FILE = "rasFm.state"

#-------------------------------------------------------------------------------
# Utility functions & classes:

def canHazInternet():
    """
    Return True if there is an active internet connection, else False.
    """
    try:
        socket.gethostbyname("www.google.com")
        return True
    except:
        return False
        
class Scroller(object):
    """
    Object designed to auto-scroll text on a LCD screen.  Every time the scroll()
    method is called to, it will scroll the text from right to left by one character
    on any line that is greater than the provided with.
    If the lines ever need to be reset \ updated, call to the setLines() method.
    """
    def __init__(self, lines=[], space = " :: ", width=16, height=2):
        """
        Instance a LCD scroller object.
        
        Parameters:
        lines : list / string : Default empty list : If a list is passed in, each 
            entry in the list is a  string that should be displayed on the LCD, 
            one line after the next.  If a string, it will be split by any embedded 
            linefeed \n characers into a list of multiple lines . 
            Ultimately, the number of entries in this list must be equal to or 
            less than the height argument.
        space : string : Default " :: " : If a given line is longer than the width
            argument, this string will be added to the end to help designate the
            end of the line has been hit during the scroll.
        width : int : Default 16 : The width of the LCD display, number of columns.
        height : int : Default 2 : the height of the LCD, number of rows.
        """
        self.width = width
        self.height = height
        self.space = space
        self.setLines(lines)
                
    def setLines(self, lines):
        """
        Set (for the first time) or reset (at any time) the lines to display.
        Sets self.lines
        
        Parameters:
        lines : list : Each entry in the list is a string
            that should be displayed on the LCD, one line after the next.  The 
            number of entries in this list must be equal to or less than the 
            height argument.
        """
        # Just in case a string is passed in, turn it into a list, and split
        # by any linefeed chars:
        if isinstance(lines, basestring):   
            lines = lines.split("\n")
        elif not isinstance(lines, list):
            raise Exception("Argument passed to lines parameter must be list, instead got: %s"%type(lines))
        if len(lines) > self.height:
            raise Exception("Have more lines to display (%s) than you have lcd rows (%s)"%(len(lines), height))            
        self.lines = lines
        # If the line is over the width, add in the extra spaces to help separate
        # the scroll:
        for i,ln in enumerate(self.lines[:]):
            if len(ln) > self.width:
                self.lines[i] = "%s%s"%(ln,self.space)
        
    def scroll(self):
        """
        Scroll the text by one character from right to left each time this is
        called to.
        
        Return : string : The message to display to the LCD.  Each line is separated
            by the \n (linefeed) character that the Adafruit LCD expects.  Each line
            will also be clipped to self.width, so as to not confuse the LCD when
            later drawn.
        """
        for i,ln in enumerate(self.lines[:]):
            if len(ln) > 16:
                shift = "%s%s"%(ln[1:], ln[0])
                self.lines[i] = shift
        truncated = [ln[:self.width] for ln in self.lines]
        return "\n".join(truncated)          

class MPC(object):
    """
    Python wraper around the mpc app.
    """
    def __init__(self):
        # fire it up!  It needs to be playing for other tools to work.
        if not ("[playing]" in self.getStatus()):
            os.system("mpc play")
            
    def getStatus(self):
        """
        Return back the current status of mpc.  
        If it's not playing, it will return a string with something like:
        "volume: 90%   repeat: off   random: off   single: off   consume: off"
        
        If it is playing, you'll get a tuple containing something like:
        ('Some Info about the song\nvolume: 90%   repeat: off   random: off   single: off   consume: off\n', 
        None)
        
        return : string : The above text, in either case.
        """
        status = Popen("mpc", stdout=PIPE).communicate()
        if isinstance(status, basestring):
            return status
        elif isinstance(status, tuple):
            return status[0]
    
    def getData(self):
        """
        Return dict of interesting data.  
        Key/values are {"playing":string, "track":int, "tracks":int, "volume":int}
        
        Note, I've only really spent time parsing SomaFM streams, so anything
        else will just have the first 16 chars displayed.
        """
        data = {"playing":None,
                "track":None,
                "tracks":None,
                "volume":None}
                
        # Get system-level mpc data, simply by calling to it and capturing the result:
        # Note, if mpc isn't playing, this won't give us everything we need.
        status = self.getStatus()
        sysdata = [f.strip() for f in status.split("\r\n") if f != ""]
        rawdata = []
        for d in sysdata:
            splitter = d.splitlines()
            rawdata.extend(splitter)
        # This should give three items, looking like this (if soma FM):
        # Lush: Mostly female vocals with an electronic influence. [SomaFM]: Love Echo - Departure                                                                        
        # [playing] #2/3   0:08/0:00 (0%)                                                                                                                                 
        # volume: 88%   repeat: off   random: off   single: off   consume: off
        
        # Get what's playing
        data["playing"] = rawdata[0]
        if "[SomaFM]" in rawdata[0]:
            # Special consideration for SomaFM playlists, since that's what' I listen to most ;)
            # For example:
            # Deep Space One: Deep ambient electronic and space music. [SomaFM]: Sync 24 - Sequor
            chopped = rawdata[0].split(":")
            line1 = ""
            line2 = ""
            if len(chopped) == 3:
                line1 = chopped[0].strip()
                line2 = chopped[2].strip()
            elif len(chopped) == 2:
                line1 = chopped[0].strip()
                line2 = chopped[1].strip()           
            data["playing"] = "%s\n%s"%(line1, line2)
        elif " - " in rawdata[0]:
            # If we find a dash, split things by dash and turn the first two into
            # what we'll display.
            chopped = rawdata[0].split(" - ")
            line1 = chopped[0].strip()
            line2 = chopped[1].strip() 
            data["playing"] = "%s\n%s"%(line1, line2)
        
        # Get which track is playing, and total number of tracks:
        data["track"] = 0
        data["tracks"] = 0
        data["volume"] = 0
        try:
            trackDataSpilt = rawdata[1].split()[1]  # "#2/3"
            track,tracks = re.split("[#/]", trackDataSpilt)[1:] # The first item is empty string
            data["track"] = int(track)
            data["tracks"] = int(tracks)
        except IndexError:
            print "Fail, don't know how to split the first index of:", rawdata
            raise
        
        # Get the volume level
        vol = rawdata[2].split()[1][:-1]
        if vol == "repeat":
            # For some reason, this will be 'repeat' if a val of 100 shows up.
            vol = 100
        data["volume"] = int(vol)
        
        return data
        
    def playTrack(self, track):
        """
        Play the passed in track number.
        """
        tracks = self.getData()["tracks"]
        if track < 1:
            track = 1
        if track > tracks:
            track = tracks
        os.system("mpc play %s"%track)
        
    def playNextTrack(self, direction):
        """
        Play the next track.  Will wrap from last to first, and first to last.
        
        Parameters:
        direction : either 1 or -1
        """
        data = self.getData()
        tracks = data["tracks"]
        track = data["track"]
        
        nextTrack = track+direction
        if nextTrack < 1:
            nextTrack = tracks
        elif nextTrack > tracks:
            nextTrack  = 1
        self.playTrack(nextTrack)
        
    def changeVolume(self, val):
        """
        Change the volume greater or less by the passed in value.
        """
        volume = self.getData()["volume"]
        volume += int(val)
        if volume < 0:
            volume = 0
        elif volume > 100:
            volume = 100
        os.system("mpc volume %s"%volume)
        
    def setVolume(self, val):
        """
        Set the volume to the value.
        """
        os.system("mpc volume %s"%int(val))
        
    def stop(self):
        """
        Quit mpc
        """
        os.system("mpc stop")

#-------------------------------------------------------------------------------
# Main application:
       
class App(Adafruit_CharLCDPlate):
    """
    Main radio application.  Call to the run() method to get things going once
    instanced.
    """
    def __init__(self):
        Adafruit_CharLCDPlate.__init__(self)
        self.mpc = None
        self.data = {}
        self.displaySet = False
        # This will help with scrolling text:
        self.displayScroll = Scroller()
        
    def startup(self):
        """
        Run one time before the main loop:  Load prefs, display welcome.
        """
        self.internetCheck()
        self.mpc = MPC()
        if not self.loadPrefs():
            self.mpc.playTrack(1)
            self.mpc.setVolume(70)
        self.clear()
        self.backlight(self.WHITE)
        self.message("RaspberryFM :-)\nVersion %s"%__version__)
        sleep(4)
        self.getData()    
        
        self.displayScroll.setLines(self.data["playing"])
        
    def run(self):
        """
        Main loop.  Call to this to launch the Application once instanced.
        """
        self.startup()
        loop = 1
        while loop:
            # sel = 1, r = 2, d = 4, u = 8, l = 16
            if self.buttons() == 17:  
                # Press Select & Left to shutdown Pi:
                self.stop('shutdown') 
            elif self.buttons() == 3:  
                # Press Select & Right to exit program:
                loop = self.stop('exit')                  
            elif self.buttonPressed(self.UP):
                self.up()
            elif self.buttonPressed(self.DOWN):
                self.down()
            elif self.buttonPressed(self.LEFT):
                self.left()
            elif self.buttonPressed(self.RIGHT):
                self.right()
            elif self.buttonPressed(self.SELECT):
                self.select()
            else:
                self.setDisplay()
                
            self.internetCheck()
            sleep(.5)
                
    #------------------------
    # Button commands:
    
    def up(self):
        """
        Increase volume
        """
        self.clear()
        self.backlight(self.GREEN)
        self.mpc.changeVolume(5)
        self.getData()
        vol = self.data["volume"]
        self.message("Volume +\n%s"%vol)
        self.savePrefs()
        self.displaySet = False
        sleep(.25)
    
    def down(self):
        """
        Decrease volume
        """
        self.clear()
        self.backlight(self.RED)
        self.mpc.changeVolume(-5)
        self.getData()
        vol = self.data["volume"]
        self.message("Volume -\n%s"%vol)
        self.savePrefs()
        self.displaySet = False
        sleep(.25)
    
    def left(self):
        """
        Go back to previous track.
        """
        self.clear()
        self.backlight(self.BLUE)
        self.mpc.playNextTrack(-1)
        self.displaySet = False
        self.getData()
        self.message("< Channel %s/%s <"%(self.data["track"], self.data["tracks"]))
        self.savePrefs()
        sleep(.5)
    
    def right(self):
        """
        Advance to next track
        """
        self.clear()
        self.backlight(self.BLUE)
        self.mpc.playNextTrack(1)
        self.displaySet = False
        self.getData()
        self.message("> Channel %s/%s >"%(self.data["track"], self.data["tracks"]))
        self.savePrefs()
        sleep(.5)
    
    def select(self, hasNet=True):
        """
        Holding select prints the text explaining how to close the app:
        Select + Left = shutdown Pi
        Select + Right = exit app (leave Pi running)
        
        Parameters:
        hasNet : bool : Default True : Determines the background color.  If False,
            make the background red, otherwise violet.
        """
        self.clear()
        if hasNet:
            self.backlight(self.VIOLET)
        else:
            self.backlight(self.RED)
        self.message("sel+L = shutdown\nsel+R = exit")
        self.displaySet = False
        sleep(1)
        
    #------------------------    
    # Util methods
    
    def internetCheck(self):
        """
        Check to see if there is internet.  If not, go into holding pattern
        until it shows up.  Allow the user to quit if they want.
        """
        hasNets = canHazInternet()
        if not hasNets:
            i = 0
            self.clear()
            self.backlight(self.RED)
            self.message("No internet :-(\nWaiting...")
            while not hasNets:
                # Detect for any button presses while we're waiting for internet
                # to return...
                # sel = 1, r = 2, d = 4, u = 8, l = 16
                if self.buttons() == 17:  
                    # Press Select & Left to shutdown Pi:
                    self.stop('shutdown') 
                elif self.buttons() == 3:  
                    # Press Select & Right to exit program:
                    self.stop('exit')  
                    raise Exception("No internet, exiting App based on user input.")
                elif self.buttonPressed(self.SELECT):
                    self.select(hasNet=False)
                # Draw message:
                dots = "."*i
                self.clear()
                self.message("No internet :-(\nWaiting%s"%dots)
                i += 1
                if i%8==0:
                    i=0
                sleep(1)
                hasNets = canHazInternet()
            self.displaySet = False
    
    def getData(self):
        """
        Get/store the latest data from mpc
        """
        self.data = self.mpc.getData()
    
    def setDisplay(self):
        """
        Displays info about the current song when no other buttons have been pressed.
        """
        # Get the last remembered song info:
        playing = self.data["playing"]
        # Get the lastest song data:
        self.getData()
        # If some other button had been pressed, this state will be False.
        # Basically pressing any other button (station change, volume change,
        # or select) will cause the display to reset when returning back to the
        # song info.
        if not self.displaySet:
            self.clear()
            self.backlight(self.TEAL)
            displayData = self.data["playing"]
            self.displayScroll.setLines(displayData)
            # Need to make sure we don't have more than 16 chars per line or
            # the (brief) display can get wonky:
            choppedData = "\n".join([item[:16] for item in displayData.split("\n")])
            self.message(choppedData)
        else:
            # If the new data is different than the old data, update the display:
            if playing != self.data["playing"]:
                displayData = self.data["playing"]
                # Update our scroller with the new text:
                self.displayScroll.setLines(displayData)
                # Display the text (before scroll starts)
                self.clear()
                self.message(self.data["playing"])
            elif playing == "":
                self.clear()
                #self.message(self.data["Missing data..."])
                self.message("Missing data...")
                print "missing data!"
            else:
                # Finally, scroll the text, and display it:
                scrolled = self.displayScroll.scroll()
                self.clear()
                self.message(scrolled)
            
        self.displaySet = True
        sleep(.2)
        
    def savePrefs(self):
        """
        Save the current prefs.  self.getData() should be ran previous.
        """
        with open(STATE_FILE, 'w') as df:
            cPickle.dump(self.data, df, 2)
            
    def loadPrefs(self):
        """
        If the pref file exists on disk, load it:
        """
        if os.path.isfile(STATE_FILE):
            with open(STATE_FILE) as sf:
                data = cPickle.load(sf)
                self.mpc.setVolume(data["volume"])
                print "loadPrefs() : LOADING TRACK PREF:", data["track"]
                self.mpc.playTrack(data["track"])
            return 1
        else:
            return 0
    
    def stop(self, mode):
        """
        Stop stop stop.
        
        If Select + Left are pressed and held, shutdown the Pi.
        If Select + Right are pressed and held, exit the program (leave Pi running).
        
        mode : Either "exit" or "shutdown"
        """
        self.clear()
        self.message("Goodbye! Thanks\nfor listening!")
        # flashy!
        for i in range(3):
            for col in (self.RED, self.YELLOW, self.GREEN, self.TEAL, self.BLUE, self.VIOLET, self.WHITE):
                self.backlight(col)
                self.mpc.changeVolume(-2)
                sleep(.05)
        self.mpc.stop()
        self.backlight(self.OFF)
        self.noDisplay()
        if mode == "exit":
            return 0    
        if mode == "shutdown":
            os.system("sudo shutdown -h now")

#-------------------------------------------------------------------------------
# When this module is called to directly, launch the App. 
if __name__ == "__main__":
    app = App().run()
    print "raspberryFm : Exit"