# Example #7 from http://picamera.readthedocs.org/en/release-1.0/quickstart.html

# This example demonstrates capturing low resolution JPEGs extremely rapidly using 
# the video-port capability of the capture_sequence() method. The framerate of the 
# captures is displayed afterward:

import time
import picamera

with picamera.PiCamera() as camera:
    camera.resolution = (640, 480)
    camera.start_preview()
    start = time.time()
    camera.capture_sequence(('image%03d.jpg' % i for i in range(120)), use_video_port=True)
    print('Captured 120 images at %.2ffps' % (120 / (time.time() - start)))
    camera.stop_preview()